# Примеры работы с инструментами анализа данных

## Структура

Репозиторий представляет собой три директории:

1. данные ([data](https://gitlab.com/iKzN/examples/-/tree/master/data))
2. примеры с теорией ([theory-solution](https://gitlab.com/iKzN/examples/-/tree/master/theory-solution/))
3. примеры их обработки в jupyter инструментами анализа данных ([ready-solution-example-notebook](https://gitlab.com/iKzN/examples/-/tree/master/ready-solution-example-notebook))

## Список примеров с теорией

1. Линейная регрессия ([linear-regressio](https://gitlab.com/iKzN/examples/-/tree/master/theory-solution/linear-regression))

## Список примеров использования инструментов обработки

1. Применения t-критерия
2. Корреляция
3. Выделение признаков временного ряда
4. Тест на стационарность и попытка предсказать с помощью ARIMA
5. Линейная регрессия
6. Случайый лес
7. Кластеризация с помощью метода k-средних
8. Визуализация на карте
9. Классификация события
